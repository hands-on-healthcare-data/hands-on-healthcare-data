{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# MIMIC and PostgreSQL\n",
    "\n",
    "This notebook contains code that imports the MIMIC data into PostgreSQL, a\n",
    "traditional, SQL-based relational database.\n",
    "\n",
    "It uses pandas/numpy to to read and process data as dataframes.  It also uses\n",
    "several functions created specifically to accompany _Hands-On Healthcare Data_.\n",
    "\n",
    "The environment variable `UMLS_APIKEY` must be set -- it can be found in your\n",
    " [UTS profile](https://uts.nlm.nih.gov/uts/edit-profile)\n",
    "\n",
    "This notebook also assumes that you have PostgreSQL running as a local docker\n",
    "container.  To make it easier, I am using a Docker compose configuration that\n",
    " also provides a pgAdmin web interface for interacting with Postgres since it\n",
    "  does not come with its own web interface by default.\n",
    "\n",
    "The forked repo is available [here](https://github\n",
    ".com/andrew-nguyen/compose-mimic-iii-pgadmin) and contains some very minor\n",
    "changes from the upstream version.  Namely, it sets several environment\n",
    "variables given the directory structure used in the book.\n",
    "\n",
    "To start the containers, you just need to run `./build.sh` from the terminal.\n",
    "The script will start the containers, wait for them to initialize, then\n",
    "start the process of copying the MIMIC data into their respective tables.\n",
    "This will start both the database server as well as the web interface.  There\n",
    " is also a GraphQL implementation"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "outputs": [],
   "source": [
    "from datetime import datetime\n",
    "import pandas as pd\n",
    "import pprint\n",
    "\n",
    "import umls_importer.umls as umls\n",
    "import database.graph as graph\n",
    "\n",
    "# Sets up autoreload of modules every time a cell is executed\n",
    "%load_ext autoreload\n",
    "%autoreload 2"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Creating a PostgreSQL connection\n",
    "\n",
    "Next, we use a helper function to establish a connection to the postgres\n",
    "instance running in our Docker environment.  The details below assume that\n",
    "the standard port (5432) is being used and exposed via _localhost_.  Please\n",
    "edit these details if you are running postgres on any other IP address or port.\n",
    "\n",
    "`username` and `password` default to \"mimic\" and \"password\" respectively.\n",
    "\n",
    "`dbname` defaults to \"mimic\".\n",
    "\n",
    "_Since the SQL instance is used as a comparator to graph-based approaches,\n",
    "the functionality is limited to `SELECT` queries.  We will not be\n",
    "manipulating any of the data within postgres._"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "outputs": [],
   "source": [
    "p = graph.PostgresDB()"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Load Medication Concepts\n",
    "\n",
    "Unlike our approach with other databases, we will not be loading the\n",
    "RxNorm-based medication concepts into postgres.  We could create a new table\n",
    "to replicate the concepts but would need to add several more tables to\n",
    "facilitate connecting these concepts to prescription entries and to create new\n",
    " medication concepts.\n",
    "\n",
    "This is certainly possible and there are many resources describing best\n",
    "practices for designing such schemas."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Mapping a New Heparin Concept\n",
    "\n",
    "As we discussed in chapter 3, there are a few different medication concepts\n",
    "that contain 'heparin' in the name.  Some of these are used to prevent clots\n",
    "in tubing/catheters and not for therapeutic purposes.  However, given the\n",
    "rigidity of SQL-based database schemas, we would need to augment the database\n",
    " to account for such additional mappings.  One such approach would be the\n",
    " [vocabulary](https://ohdsi.github.io/TheBookOfOhdsi/StandardizedVocabularies.html) tables used by the OMOP common data model.\n",
    "\n",
    "For the SQL-based discussion, we will not create a new concept but simply\n",
    "embed this \"knowledge\" directly into the queries themselves."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Though our database does not support the simple creation and management of\n",
    "new concepts, we can still extract this information the same way as we did\n",
    "in the MIMIC/ArangoDB example.  Given the NDC to RxCUI mapping document, we\n",
    "identified the following RxCUI's to be those that we would like to use to\n",
    "identify patients who received Heparin as an anticoagulant treatment.\n",
    "\n",
    "848335\n",
    "1361615\n",
    "1362831\n",
    "1658717\n",
    "\n",
    "Using the same CSV file, we can also identify associated NDC codes:"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "RXCUI Concepts shape: (997, 7)\n",
      "NDCs of interest:\n",
      "['63323026201', '00338055002', '00641040025', '00074779362', '00264958720', '00409779362', '63323054207']\n"
     ]
    }
   ],
   "source": [
    "rxcuis_to_map = [\n",
    "    \"848335\",\n",
    "    \"1361615\",\n",
    "    \"1362831\",\n",
    "    \"1658717\"\n",
    "]\n",
    "\n",
    "data = umls.read_rxcui_csv('mimic_1.4_demo_drugs_rxcui.csv')\n",
    "print(f\"RXCUI Concepts shape: {data.shape}\")\n",
    "\n",
    "hep = data[data['rxcui'].isin(rxcuis_to_map)]\n",
    "hep_ndcs = hep['ndc'].tolist()\n",
    "print('NDCs of interest:')\n",
    "print(hep_ndcs)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Now, we need to query for those prescriptions that are related to these NDCs:"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found 223 prescriptions\n"
     ]
    }
   ],
   "source": [
    "sql = \"SELECT * FROM prescriptions WHERE ndc IN %s\"\n",
    "results = p.execute(sql, (tuple(hep_ndcs),))\n",
    "print(f\"Found {len(results)} prescriptions\")"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "As you can see, this process isn't necessarily any more complex than how we\n",
    "handled this with graph databases.  However, the _knowledge_ about which\n",
    "NDC's were used to identify the 223 prescriptions of interest are embedded\n",
    "within this code above.  There is no good way to share this information with\n",
    "other data analysts, scientists, or engineers.\n",
    "\n",
    "Imagine the following scenario:\n",
    "\n",
    "You are working on this project and have identified the list of 4 RxNorm\n",
    "concepts you would like to map.  Perhaps this is from your own analysis of\n",
    "the data or from an external vendor's mapping file.  You follow the process\n",
    "above and identify the prescriptions of interest.  You end up sharing this\n",
    "code with a handful of colleagues and your mappings get used in quite a few\n",
    "analyses.\n",
    "\n",
    "One of your colleagues notices that the list has an error given that the data\n",
    " were collected years ago but you used the current mapping of RxNorm to NDC.\n",
    "  Since codes often added/removed, your list has an error.\n",
    "\n",
    "How do you disseminate the new list and communicate that existing analyses\n",
    "may contain errors?  At a smaller company, this might not be too much of a\n",
    "hassle.  However, at a multinational pharma company with thousands of data\n",
    "scientists, this is a much more difficult challenge.\n",
    "\n",
    "By pulling these mappings out of the source code and analysis plans, we can\n",
    "begin to manage this process and ensure that our analyses are reproducible\n",
    "and maintainable.  This is particularly important as we integrate increasing\n",
    "amounts of real world data.  RWD is extremely messy and we may find the need\n",
    "to remap the data as we learn more about the source and context of the data,\n",
    "the scientific question, or even the underlying disease process."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}