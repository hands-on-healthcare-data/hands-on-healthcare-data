The examples in this repository assume that this directory contains the CSV 
files from the 
[MIMIC-III Clincial Database Demo](https://doi.org/10.13026/C2HM2Q).

The CSV files should be unzipped directly into this directory (i.e., there 
should not be any subdirectories).

SHA-256 hashes for v1.4 of the database demo are included.

A second copy is hosted as a git repo [here](https://gitlab.com/hands-on-healthcare-data/mimic-iii-clinical-demo-database).