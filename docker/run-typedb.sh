docker run \
    --name umls-typedb \
    -p 1729:1729 \
    -d \
    -v $PWD/typedb:/typedb-all-linux/server/data/ \
    vaticle/typedb:latest