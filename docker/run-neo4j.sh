docker run \
    --name umls-neo4j \
    -p7474:7474 -p7687:7687 \
    -d \
    -v $PWD/neo4j/data:/data \
    -v $PWD/neo4j/logs:/logs \
    -v $PWD/neo4j/import:/var/lib/neo4j/import \
    -v $PWD/neo4j/plugins:/plugins \
    --env=NEO4J_dbms_memory_heap_initial__size=8g \
    --env=NEO4J_dbms_memory_heap_max__size=8g \
    --env=NEO4J_AUTH=neo4j/test \
    --env NEO4J_apoc_import_file_enabled=true \
    --env=NEO4JLABS_PLUGINS='["apoc", "graph-data-science", "n10s"]' \
    neo4j:4.4-community