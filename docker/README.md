# Docker Resources

These scripts are intended to be run via CLI to create Docker containers used in
[Hands-On Healthcare Data](https://www.oreilly.com/library/view/hands-on-healthcare-data/9781098112912/).

If you would like to use Docker Compose instead, please check out this
[repo](https://gitlab.com/hands-on-healthcare-data/exercises-ingesting-data).
