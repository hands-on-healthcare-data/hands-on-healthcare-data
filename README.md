# Hands-On Healthcare Data

This repository contains source code, examples, and other related materials to
accompany the book [Hands-On Healthcare Data](https://www.oreilly.com/library/view/hands-on-healthcare-data/9781098112912/).

As with any open source project, the code contained within this repository is
intended to benefit the greater health informatics community and I appreciate
any comments, feedback, and pull requests.

## Repository Structure

At the top-level of this repo, there are several directories:

```
hands-on-healthcare-data/
├─ README.md (this file)
├─ data/
│  ├─ mimic-iii-clinical-database-demo/
├─ notebooks/
│  ├─ mimic-neo4j/
│  ├─ mimic-sqlite/
|  ├─ mimic-typedb/
│  │
│  ├─ umls-neo4j/
│  ├─ umls-typedb/
│  │
│  ├─ umls-authentication/
│  ├─ umls-rxnorm-prescriptions/
```

### Chapter 3 – Standardized Vocabularies in Healthcare

This chapter introduces concepts around standardized vocabularies such as the 
Unified Medical Language System (UMLS) and references the following notebooks:

- **umls-authentication** – How to authenticate with the UMLS web service API
- **umls-typedb** – How to load the UMLS into TypeDB.  _Note that there are no 
notebooks for loading the UMLS into SQLite or Neo4j because those processes do 
not involve any Python code._ 
- **umls-rxnorm-prescriptions** – How to look up prescription data using the 
UMLS web service API

### Chapter 4 – Deep Dive: Electronic Health Record Data

This chapter demonstrates how to work with EHR data using the MIMIC-III clinical
database demo v1.4.  It references the following notebooks:

- **mimic-neo4j** – How to load the MIMIC-III database into Neo4j (property 
graph)
- **mimic-sqlite** – How to load the MIMIC-III database into SQLite (SQL 
database)
- **mimic-typedb** – How to load the MIMIC-III database into TypeDB 
(hypergraph / strongly-typed database)

## License

Copyright © 2021 Andrew Nguyen

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.